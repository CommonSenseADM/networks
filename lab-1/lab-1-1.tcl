set ns [new Simulator]
set nf [open out.nam w]
$ns namtrace-all $nf

#============================================================

#==================================================
#     Nodes
#==================================================
for {set index 1} {$index <= 3} {incr index} {
	set s($index) [$ns node]
}
for {set index 1} {$index <= 5} {incr index} {
	set r($index) [$ns node]
}
for {set index 1} {$index <= 3} {incr index} {
	set k($index) [$ns node]
}

#==================================================
#     Senders / Receivers
#==================================================
set udp(1) [new Agent/UDP]
$ns attach-agent $s(1) $udp(1)
set cbr(1) [new Application/Traffic/CBR]
$cbr(1) set packetSize_ 300
$cbr(1) set interval_ 0.005
$cbr(1) attach-agent $udp(1)

set null(1) [new Agent/Null]
$ns attach-agent $k(1) $null(1)

$ns connect $udp(1) $null(1)

#==================================================
#     Scheme
#==================================================
$ns duplex-link $s(1) $r(1) 2Mb 5ms DropTail
$ns duplex-link $r(1) $s(2) 2Mb 5ms DropTail
$ns duplex-link $r(1) $s(3) 2Mb 5ms DropTail
$ns duplex-link $r(1) $r(2) 1Mb 5ms DropTail
$ns duplex-link $r(2) $r(3) 2Mb 5ms DropTail
$ns duplex-link $r(2) $r(4) 2Mb 5ms DropTail
$ns duplex-link $r(3) $k(1) 2Mb 5ms DropTail
$ns duplex-link $r(3) $r(5) 2Mb 5ms DropTail
$ns duplex-link $r(4) $r(5) 2Mb 5ms DropTail
$ns duplex-link $r(5) $k(2) 2Mb 5ms DropTail
$ns duplex-link $r(5) $k(3) 2Mb 5ms DropTail

$ns duplex-link-op $s(1) $r(1) orient down
$ns duplex-link-op $r(1) $s(2) orient left
$ns duplex-link-op $r(1) $s(3) orient down
$ns duplex-link-op $r(1) $r(2) orient right
$ns duplex-link-op $r(2) $r(3) orient right
$ns duplex-link-op $r(2) $r(4) orient right-down
$ns duplex-link-op $r(3) $k(1) orient right-up
$ns duplex-link-op $r(3) $r(5) orient right-down
$ns duplex-link-op $r(4) $r(5) orient right
$ns duplex-link-op $r(5) $k(2) orient right-up
$ns duplex-link-op $r(5) $k(3) orient right

#==================================================
#     Timings
#==================================================
$ns at 0.1 "$cbr(1) start"
$ns at 5.0 "$cbr(1) stop"
$ns at 8.0 "finish"

#============================================================

proc finish {} {
	global ns nf
	$ns flush-trace
	close $nf
	exec nam out.nam
	exit 0
}

$ns run
