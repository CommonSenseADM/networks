proc attach-expoo-traffic { node sink size burst idle rate} {
	set ns [Simulator instance]
	set source [new Agent/CBR/UDP]
	$ns attach-agent $node $source
	set traffic [new Traffic/Expoo]
	$traffic set packet-size $size
	$traffic set burst-time $burst
	$traffic set idle-time $idle
	$traffic set rate $rate
	$source attach-traffic $traffic
	$ns connect $source $sink
	return $source
}

proc record {} {
	global sink f
	set ns [Simulator instance]
	set time 0.5
# bytes_ = число полученных байт
	set bw(0) [$sink(0) set bytes_]
	set bw(1) [$sink(1) set bytes_]
	set bw(2) [$sink(2) set bytes_]
	set now [$ns now]
	puts $f(0) "$now [expr $bw(0)/$time*8/1000000]"
	puts $f(1) "$now [expr $bw(1)/$time*8/1000000]"
	puts $f(2) "$now [expr $bw(2)/$time*8/1000000]"
	$sink(0) set bytes_ 0
	$sink(1) set bytes_ 0
	$sink(2) set bytes_ 0
	$ns at [expr $now+$time] "record"
}

#============================================================

set ns [new Simulator]
set nf [open out.nam w]
$ns namtrace-all $nf

#============================================================

#==================================================
#     Colors
#==================================================
$ns color 1 Red
$ns color 2 Green
$ns color 3 Blue

#==================================================
#     Monitor files
#==================================================
set f(0) [open out0.tr w]
set f(1) [open out1.tr w]
set f(2) [open out2.tr w]

#==================================================
#     Nodes
#==================================================
for {set index 0} {$index <= 4} {incr index} {
	set n($index) [$ns node]
}

#==================================================
#     Senders / Receivers
#==================================================
set sink(0) [new Agent/LossMonitor]
set sink(1) [new Agent/LossMonitor]
set sink(2) [new Agent/LossMonitor]

$ns attach-agent $n(4) $sink(0)
$ns attach-agent $n(4) $sink(1)
$ns attach-agent $n(4) $sink(2)

set source(0) [attach-expoo-traffic $n(0) $sink(0) 200 2s 1s 100k]
set source(1) [attach-expoo-traffic $n(1) $sink(1) 200 2s 1s 200k]
set source(2) [attach-expoo-traffic $n(2) $sink(2) 200 2s 1s 300k]

$source(0) set fid_ 1
$source(1) set fid_ 2
$source(2) set fid_ 3

#==================================================
#     Scheme
#==================================================
$ns duplex-link $n(0) $n(3) 1Mb 100ms DropTail
$ns duplex-link $n(1) $n(3) 1Mb 100ms DropTail
$ns duplex-link $n(2) $n(3) 1Mb 100ms DropTail
$ns duplex-link $n(3) $n(4) 1Mb 100ms DropTail

$ns duplex-link-op $n(0) $n(3) orient right-down
$ns duplex-link-op $n(1) $n(3) orient right
$ns duplex-link-op $n(2) $n(3) orient right-up
$ns duplex-link-op $n(3) $n(4) orient right

#==================================================
#     Queue
#==================================================
$ns duplex-link-op $n(3) $n(4) queuePos 0.5

#==================================================
#     Timings
#==================================================
$ns at 0.0 "record"
$ns at 10.0 "$source(0) start"
$ns at 10.0 "$source(1) start"
$ns at 10.0 "$source(2) start"
$ns at 50.0 "$source(0) stop"
$ns at 50.0 "$source(1) stop"
$ns at 50.0 "$source(2) stop"
$ns at 60.0 "finish"

#============================================================

proc finish {} {
	global f
	close $f(0)
	close $f(1)
	close $f(2)
	exec xgraph out0.tr out1.tr out2.tr -geometry 800x600 \-0 source0 -1 source1 -2 source2 &
	exit 0
}

$ns run
