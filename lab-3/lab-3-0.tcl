set ns [new Simulator]
set nf [open out.nam w]
$ns namtrace-all $nf

#============================================================

#==================================================
#     Colors
#==================================================
$ns color 1 Blue

#==================================================
#     Nodes
#==================================================
set s(1) [$ns node]
set r(1) [$ns node]
set r(2) [$ns node]

#==================================================
#     Senders / Receivers
#==================================================
set tcp(1) [new Agent/TCP]
$tcp(1) set fid_ 1
$tcp(1) set maxcwnd_ 50
$tcp(1) set packetSize_ 200
$ns attach-agent $s(1) $tcp(1)
set ftp(1) [$tcp(1) attach-source FTP]

set snk(1) [new Agent/TCPSink]
$ns attach-agent $r(2) $snk(1)

$ns connect $tcp(1) $snk(1)

#==================================================
#     Scheme
#==================================================
$ns duplex-link $s(1) $r(1) 256kb 150ms DropTail
$ns duplex-link $s(1) $r(2) 256kb 150ms DropTail
$ns duplex-link $r(1) $r(2) 256kb 150ms DropTail

$ns duplex-link-op $s(1) $r(1) orient right-up
$ns duplex-link-op $s(1) $r(2) orient right
$ns duplex-link-op $r(1) $r(2) orient right-down

#==================================================
#     Queue
#==================================================
$ns queue-limit $s(1) $r(2) 10

#==================================================
#     Timings
#==================================================
$ns at 0.1 "$ftp(1) produce 70"
$ns rtmodel-at 2.0 down $s(1) $r(2)
$ns rtmodel-at 3.0 up $s(1) $r(2)
$ns at 6.0 "finish"

#============================================================

proc finish {} {
	global ns nf
	$ns flush-trace
	close $nf
	exec nam out.nam
	exit 0
}

$ns run
