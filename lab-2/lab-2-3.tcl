set ns [new Simulator]
set nf [open out.nam w]
$ns namtrace-all $nf

#============================================================

#==================================================
#     Colors
#==================================================
$ns color 1 Blue
$ns color 2 Red

#==================================================
#     Nodes
#==================================================
for {set index 1} {$index <= 3} {incr index} {
	set s($index) [$ns node]
}
for {set index 1} {$index <= 5} {incr index} {
	set r($index) [$ns node]
}
for {set index 1} {$index <= 3} {incr index} {
	set k($index) [$ns node]
}

#==================================================
#     Senders / Receivers
#==================================================
set tcp(1) [new Agent/TCP]
$ns attach-agent $s(1) $tcp(1)
$tcp(1) set packetSize_ 100
$tcp(1) set maxcwnd_ 50
$tcp(1) set fid_ 1
set ftp(1) [$tcp(1) attach-source FTP]
set snk(1) [new Agent/TCPSink]
$ns attach-agent $k(1) $snk(1)
$ns connect $tcp(1) $snk(1)

#--------------------------------------------------
#     CHANGES
#--------------------------------------------------
set tfile [new Tracefile]
$tfile filename lab-2/trace.dat
set src(2) [new Agent/CBR/UDP]
$ns attach-agent $s(2) $src(2)
$src(2) set fid_ 2
set trace [new Traffic/Trace]
$trace attach-tracefile $tfile
$src(2) attach-traffic $trace
set null(2) [new Agent/Null]
$ns attach-agent $k(2) $null(2)
$ns connect $src(2) $null(2)
#--------------------------------------------------

#==================================================
#     Scheme
#==================================================
$ns duplex-link $s(1) $r(1) 128kb 10ms DropTail
$ns duplex-link $r(1) $s(2) 128kb 10ms DropTail
$ns duplex-link $r(1) $s(3) 128kb 10ms DropTail
$ns duplex-link $r(1) $r(2) 128kb 10ms DropTail
$ns duplex-link $r(2) $r(3) 128kb 10ms DropTail
$ns duplex-link $r(2) $r(4) 128kb 10ms DropTail
$ns duplex-link $r(3) $k(1) 128kb 10ms DropTail
$ns duplex-link $r(3) $r(5) 128kb 10ms DropTail
$ns duplex-link $r(4) $r(5) 128kb 10ms DropTail
$ns duplex-link $r(5) $k(2) 128kb 10ms DropTail
$ns duplex-link $r(5) $k(3) 128kb 10ms DropTail

$ns duplex-link-op $s(1) $r(1) orient down
$ns duplex-link-op $r(1) $s(2) orient left
$ns duplex-link-op $r(1) $s(3) orient down
$ns duplex-link-op $r(1) $r(2) orient right
$ns duplex-link-op $r(2) $r(3) orient right
$ns duplex-link-op $r(2) $r(4) orient right-down
$ns duplex-link-op $r(3) $k(1) orient right-up
$ns duplex-link-op $r(3) $r(5) orient right-down
$ns duplex-link-op $r(4) $r(5) orient right
$ns duplex-link-op $r(5) $k(2) orient right-up
$ns duplex-link-op $r(5) $k(3) orient right

#==================================================
#     Queue
#==================================================
$ns duplex-link-op $r(1) $r(2) queuePos 0.5
$ns queue-limit $r(1) $r(2) 10

#==================================================
#     Timings
#==================================================
$ns at 0.1 "$ftp(1) produce 100"
$ns at 0.2 "$src(2) start"
$ns at 8.0 "finish"

#============================================================

proc finish {} {
	global ns nf
	$ns flush-trace
	close $nf
	exec nam out.nam
	exit 0
}

$ns run
