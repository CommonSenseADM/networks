proc finish {label mod} {
	exec rm -f temp.rands
	set f [open temp.rands w]
	puts $f "TitleText: $label"
	puts $f "Device: Postscript"

	exec rm -f temp.p
	exec touch temp.p
	exec awk {
	{
		if (($1 == "+" || $1 == "-") && ($5 == "tcp" || $5 == "ack")) \
			print $2, ($8 - 1) * (mod + 10) + ($11 % mod)
		}
	} mod=$mod out.tr > temp.p

	exec rm -f temp.p2
	exec touch temp.p2
	exec awk {
	{
		if (($1 == "-") && ($5 == "tcp" || $5 == "ack")) \
			print $2, ($8 - 1) * (mod + 10) + ($11 % mod)
		}
	} mod=$mod out2.tr > temp.p2

	exec rm -f temp.d
	exec touch temp.d
	exec awk {
	{
		if ($1 == "d") \
			print $2, ($8 - 1) * (mod + 10) + ($11 % mod)
		}
	} mod=$mod out.tr > temp.d

	puts $f \"packets
	exec cat temp.p >@ $f
	puts $f \n\"acks
	exec cat temp.p2 >@ $f

	puts $f [format "\n\"skip-1\n0 1\n\n"]

	puts $f \"drops
	exec cat temp.d >@ $f
	close $f

	set tx "time (sec)"
	set ty "packet number (mod $mod)"

	exec xgraph -geometry 1000x800 -bb -tk -nl -m -zg 0 -x $tx -y $ty temp.rands &
	exit 0
}

#============================================================

set ns [new Simulator]
set nf [open out.nam w]
$ns namtrace-all $nf

set label "tcp/ftp+telnet"
set mod 80

exec rm -f out.tr
set fout [open out.tr w]
exec rm -f out2.tr
set fout2 [open out2.tr w]

$ns color 1 Blue

#============================================================

#==================================================
#     Nodes
#==================================================
for {set index 1} {$index <= 3} {incr index} {
	set s($index) [$ns node]
}
for {set index 1} {$index <= 5} {incr index} {
	set r($index) [$ns node]
}
for {set index 1} {$index <= 3} {incr index} {
	set k($index) [$ns node]
}

#==================================================
#     Senders / Receivers
#==================================================
set snk(1) [new Agent/TCPSink]
$ns attach-agent $k(2) $snk(1)
set tcp(1) [new Agent/TCP]
$tcp(1) set maxcwnd_ 15
$tcp(1) set packetSize_ 100
$ns attach-agent $s(1) $tcp(1)
set ftp(1) [$tcp(1) attach-source FTP]
$ns connect $tcp(1) $snk(1)
$tcp(1) set fid_ 1

#==================================================
#     Scheme
#==================================================
$ns duplex-link $s(1) $r(1) 2Mb 10ms DropTail
$ns duplex-link $r(1) $s(2) 2Mb 10ms DropTail
$ns duplex-link $r(1) $s(3) 2Mb 10ms DropTail
$ns duplex-link $r(1) $r(2) 256kb 200ms DropTail
$ns duplex-link $r(2) $r(3) 2Mb 10ms DropTail
$ns duplex-link $r(2) $r(4) 2Mb 10ms DropTail
$ns duplex-link $r(3) $k(1) 2Mb 10ms DropTail
$ns duplex-link $r(3) $r(5) 2Mb 10ms DropTail
$ns duplex-link $r(4) $r(5) 2Mb 10ms DropTail
$ns duplex-link $r(5) $k(2) 2Mb 10ms DropTail
$ns duplex-link $r(5) $k(3) 2Mb 10ms DropTail

$ns duplex-link-op $s(1) $r(1) orient down
$ns duplex-link-op $r(1) $s(2) orient left
$ns duplex-link-op $r(1) $s(3) orient down
$ns duplex-link-op $r(1) $r(2) orient right
$ns duplex-link-op $r(2) $r(3) orient right
$ns duplex-link-op $r(2) $r(4) orient right-down
$ns duplex-link-op $r(3) $k(1) orient right-up
$ns duplex-link-op $r(3) $r(5) orient right-down
$ns duplex-link-op $r(4) $r(5) orient right
$ns duplex-link-op $r(5) $k(2) orient right-up
$ns duplex-link-op $r(5) $k(3) orient right

#==================================================
#     Queue
#==================================================
$ns queue-limit $r(1) $r(2) 6
$ns duplex-link-op $r(1) $r(2) queuePos 0.5
$ns trace-queue $r(1) $r(2) $fout
$ns trace-queue $r(2) $r(1) $fout2

#==================================================
#     Timings
#==================================================
$ns at 0.1 "$ftp(1) produce 200"

$ns at 6.0 "ns flush-trace; close $nf; close $fout; close $fout2; finish $label $mod"

#============================================================

$ns run
