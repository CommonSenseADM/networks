set ns [new Simulator]
set nf [open out.nam w]
$ns namtrace-all $nf

set s1 [$ns node]
set r1 [$ns node]

$ns duplex-link $s1 $r1 2Mb 5ms DropTail

# Это работает
set udp1 [new Agent/UDP]
$ns attach-agent $s1 $udp1
set cbr1 [new Application/Traffic/CBR]
$cbr1 set packetSize_ 200
$cbr1 set interval_ 0.005
$cbr1 attach-agent $udp1

# Это не работает
# Код из методички
# set cbr1 [new Agent/CBR]
# $ns attach-agent $s1 $cbr1
# $cbr1 set packetSize_ 200
# $cbr1 set interval_ 0.005

set null1 [new Agent/Null]
$ns attach-agent $r1 $null1

$ns connect $udp1 $null1

proc finish {} {
	global ns nf
	$ns flush-trace
	close $nf
	exec nam out.nam
	exit 0
}

$ns at 0.5 "$cbr1 start"
$ns at 4.5 "$cbr1 stop"
$ns at 5.0 "finish"

$ns run
